# ZSH ALIASES FOR ODOO WITH VAGRANT

### INSTALL

### [Oh-my-zsh](https://github.com/robbyrussell/oh-my-zsh)

1. Clone this repository in oh-my-zsh's plugins directory:

    ```
    git clone https://bitbucket.org/ecoitecanalytics/zsh-aliases.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-aliases
    ```

2. Activate the plugin in `~/.zshrc`:

    ```
    plugins=( [plugins...] zsh-aliases)
    ```

3. Restart zsh (such as by opening a new instance of your terminal emulator).

### COMMANDS

+ **p/vtail**: Muestra el tail de la instancia ejecutada en ese vagrant
+ **p/vreload**: Recarga el odoo de la instancia
+ **p/vkill**: Detiene el servidor odoo de la instancia
+ **p/vconf**: Configura el usuario odoo para poder trabajar con los siguientes comandos (solo ejecutar la primera vez)
+ **p/vodoo [base de datos] [modulo]**: Actualiza el modulo escrito en la base de datos seleccionada de la instancia
+ **p/vall [base de datos]**: Actualiza TODOS los módulos del erp en una base de datos seleccionada
+ **p/vshell [base de datos]**: Inicia la shell del odoo con la base de datos especificada 
+ **gitupdate [rama git]**: Actualiza todos los módulos de una carpeta a la última versión en la rama seleccionada
