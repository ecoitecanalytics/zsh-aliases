
alias addons="/usr/lib/python2.7/dist-packages/odoo/addons/"


function upgrade_oh_my_zsh() {
  source $ZSH/tools/upgrade.sh

  printf "\n${BLUE}%s${NORMAL}\n" "Updating custom plugins"
  cd $ZSH/custom/plugins

  for plugin in */; do
    if [ -d "$plugin/.git" ]; then
       printf "${YELLOW}%s${NORMAL}\n" "${plugin%/}"
       git -C "$plugin" pull
    fi
  done
  cd

}


#######################
### LOCAL VARIABLES ###
#######################

SUPERTAIL="sudo tail -f /var/log/odoo/odoo-server.log | sed --unbuffered -e 's/\(.*ERROR*\)/\o033[41m\o033[1m\1\o033[40m\o033[0m/' | sed --unbuffered -e 's/\(.*Modules loaded.*\)/\o033[1;32m\1\o033[39m\o033[40m\o033[0m/' | sed --unbuffered -e 's/\(.*DEBUG*\)/\o033[39m\o033[1;36m\1\o033[40m\o033[0m/' | sed --unbuffered -e 's/\(.*odoo.addons.base.ir.ir_cron*\)/\o033[43m\o033[1m\1\o033[40m\o033[0m/' | sed --unbuffered -e 's/\(.*WARNING*\)/\o033[39m\o033[1;33m\1\o033[40m\o033[0m/'"



###################
### GIT COMMAND ###
###################


# Actualización de módulos en una carpeta
# - Uso: gitupdate [rama git]
function gitupdate(){
    version=$1
    echo $version
    for i in $(ls)
    do
      echo 'Updating: '$i
      git -C './'$i fetch --all
      git -C './'$i reset --hard origin/$version
    done
}


###########################
### PRODUCTION COMMANDS ###
###########################

# Visualizar log odoo en tiempo real
alias ptail="$SUPERTAIL"

# Reiniciar servicio odoo
alias preload="sudo service odoo restart"

# Parar servicio odoo
alias pkill="sudo killall odoo"


# Configuracion inicial odoo
function pconf(){
      sudo sed -i.bak s/'\/var\/lib\/odoo:\/usr\/sbin\/nologin'/'\/var\/lib\/odoo:\/bin\/bash'/g /etc/passwd;
      sudo sed -i.bak s/'\/var\/lib\/odoo:\/bin\/false'/'\/var\/lib\/odoo:\/bin\/bash'/g /etc/passwd;
}

# Actualizar modulo odoo
# - Uso: podoo [base de datos] [nombre modulo]
function podoo(){
        db="$1"
        module="$2"
        sudo su - odoo -c "odoo --config=/etc/odoo/odoo.conf --logfile=/var/log/odoo/odoo-server.log --dev=all -d $db -u $module"
}

# Actualizar TODOS LOS MÓDULOS
# - Uso: pall [base de datos]
function pall(){
        db="$1"
        sudo su - odoo -c "odoo --config=/etc/odoo/odoo.conf --logfile=/var/log/odoo/odoo-server.log --dev=all -d $db --update=all"
}


# Conectarse a terminal odoo
# - Uso: pshell [base de datos]
function pshell(){
        db="$1"
      echo "Conectando..."
        sudo su - odoo -c "odoo shell --config=/etc/odoo/odoo.conf --logfile=/var/log/odoo/odoo-server.log --dev=all -d $db"
}


########################
### VAGRANT COMMANDS ###
########################

# Reiniciar servicio odoo
alias vreload="vagrant ssh -c \"sudo service odoo restart\""
# Parar servicio odoo
alias vkill="vagrant ssh -c \"sudo killall odoo\""

# Visualizar log odoo en tiempo real
alias vtail="vagrant ssh -c \"$SUPERTAIL\""


# Configuracion inicial odoo
function vconf(){
      vagrant ssh -c "sudo sed -i.bak s/'\/var\/lib\/odoo:\/usr\/sbin\/nologin'/'\/var\/lib\/odoo:\/bin\/bash'/g /etc/passwd;"
      vagrant ssh -c "sudo sed -i.bak s/'\/var\/lib\/odoo:\/bin\/false'/'\/var\/lib\/odoo:\/bin\/bash'/g /etc/passwd;"
}

# Actualizar modulo odoo
# - Uso: vodoo [base de datos] [nombre modulo]
function vodoo(){
        db="$1"
        module="$2"
        vagrant ssh -c "sudo su - odoo -c \"odoo --config=/etc/odoo/odoo.conf --logfile=/var/log/odoo/odoo-server.log --dev=all -d $db -u $module\""
}

# Actualizar TODOS LOS MÓDULOS
# - Uso: vall [base de datos]
function vall(){
        db="$1"
        vagrant ssh -c "sudo su - odoo -c \"odoo --config=/etc/odoo/odoo.conf --logfile=/var/log/odoo/odoo-server.log --dev=all -d $db --update=all\""
}

# Conectarse a terminal odoo
# - Uso: vshell [base de datos]
function vshell(){
        db="$1"
      echo "Conectando..."
        vagrant ssh -c "sudo su - odoo -c \"odoo shell --config=/etc/odoo/odoo.conf --logfile=/var/log/odoo/odoo-server.log --dev=all -d $db\""
}
